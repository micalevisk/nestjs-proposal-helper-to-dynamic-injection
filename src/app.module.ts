import { BullModule, InjectQueue, getQueueToken } from '@nestjs/bull';
import { Inject, Module, InjectionToken } from '@nestjs/common'

const token: InjectionToken = 'usecase'

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'foo',
    }),
  ],

  providers: [
    {
      provide: token,
      inject: [
        // instead of this:
        getQueueToken('foo'),
        // we would use this:
        InjectionToken.of(InjectQueue('foo')),
      ],
      useFactory: (a, b) => a === b // true
    }
  ]
})
export class AppModule {
  constructor(
    @InjectQueue('foo') readonly a: any,

    // instead of this:
    @Inject( getQueueToken('foo') ) b: any,

    // use this:
    @Inject( InjectionToken.of(InjectQueue('foo')) ) c: any

    // other examples:
    /*
    @Inject( InjectionToken.of(InjectDataSource("albumsConnection") ) typeormExample: any,

    @Inject( InjectionToken.of(InjectModel(Cat.name)) ) mongooseExample: any,
    */
  ) {
    console.log(a === b) // true
    console.log(a === c) // true
  }
}
